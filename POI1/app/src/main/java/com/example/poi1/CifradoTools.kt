package com.example.poi1

import android.os.Build
import androidx.annotation.RequiresApi
import java.util.*
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

//para que sea yba clase estatica se cambia a object
object CifradoTools {
    var cifradoYesOrNot = "true"
    private const val TRANSFORM_CIPHER = "AES/CBC/PKCS5PADDING"
    //AES
    @RequiresApi(Build.VERSION_CODES.O)
    fun cifrar(textoPlano: String, LaLlave: String): String {
        val cipher = Cipher.getInstance(TRANSFORM_CIPHER)
        val llaveBytesFinal = ByteArray(16)
        val llaveBytesOriginal = LaLlave.toByteArray(charset("UTF-8"))
        System.arraycopy(
                llaveBytesOriginal,
                0,
                llaveBytesFinal,
                0,
                Math.min(
                        llaveBytesOriginal.size,
                        llaveBytesFinal.size
                )
        )

        val llaveSecretaNadieSabe: SecretKeySpec = SecretKeySpec(
                llaveBytesFinal,
                "AES"
        )
        /**vector de inicializacion**/
        val VecIni = IvParameterSpec(llaveBytesFinal)
        cipher.init(
                Cipher.ENCRYPT_MODE,
                llaveSecretaNadieSabe,
                VecIni
        )
        val textCifrado = cipher.doFinal(
                textoPlano.toByteArray(
                charset("UTF-8")
                )
        )
        var resultado = Base64.getEncoder().encodeToString(textCifrado)

        return resultado

    }
    @RequiresApi(Build.VERSION_CODES.O)
    fun descifrar(textoCifrado: String, LaLlave: String):String {
        val desifrarBytes = Base64.getDecoder().decode(textoCifrado)
        val cipher = Cipher.getInstance(TRANSFORM_CIPHER)
        val llaveBytesFinal = ByteArray(16)
        val llaveBytesOriginal = LaLlave.toByteArray(charset("UTF-8"))
        System.arraycopy(
                llaveBytesOriginal,
                0,
                llaveBytesFinal,
                0,
                Math.min(
                        llaveBytesOriginal.size,
                        llaveBytesFinal.size
                )
        )

        val llaveSecretaNadieSabe: SecretKeySpec = SecretKeySpec(
                llaveBytesFinal,
                "AES"
        )
        /**vector de inicializacion**/
        val VecIni = IvParameterSpec(llaveBytesFinal)
        cipher.init(
                Cipher.DECRYPT_MODE,
                llaveSecretaNadieSabe,
                VecIni
        )

        val textoPlano = String(cipher.doFinal(desifrarBytes))

        return textoPlano
    }
}