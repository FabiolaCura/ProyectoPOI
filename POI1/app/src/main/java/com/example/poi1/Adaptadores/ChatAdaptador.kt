package com.example.poi1.Adaptadores

import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.poi1.MisDatos
import com.example.poi1.Modelo.Mensaje
import com.example.poi1.R
import com.example.poi1.SusDatos
import java.text.SimpleDateFormat
import java.util.*

class ChatAdaptador(private val listaMensajes: MutableList<Mensaje>):
        RecyclerView.Adapter<ChatAdaptador.ChatViewHolder>() {

    class ChatViewHolder(itemViwe:View): RecyclerView.ViewHolder(itemViwe){
        fun asignarInformacion(mensaje: Mensaje) {
            if(mensaje.de == SusDatos.ID)
                itemView.findViewById<TextView>(R.id.txt_m_nombre).text = SusDatos.Nombre
            else
                itemView.findViewById<TextView>(R.id.txt_m_nombre).text = MisDatos.Nombre
            itemView.findViewById<TextView>(R.id.txt_m_mensaje).text = mensaje.contenido
            val dateFormater = SimpleDateFormat("dd/MM/yyyy - hh:mm:ss", Locale.getDefault())
            val fecha = dateFormater.format(Date(mensaje.timeStamp as Long))
            itemView.findViewById<TextView>(R.id.txt_m_fecha).text = fecha
            val params = itemView.findViewById<LinearLayout>(R.id.contenedorMensaje).layoutParams
            if(mensaje.de == MisDatos.id){
                val newParams = FrameLayout.LayoutParams(
                    params.width,
                    params.height,
                    Gravity.END
                )
                itemView.findViewById<LinearLayout>(R.id.contenedorMensaje).layoutParams = newParams
            } else {

                val newParams = FrameLayout.LayoutParams(
                    params.width,
                    params.height,
                    Gravity.START
                )
                itemView.findViewById<LinearLayout>(R.id.contenedorMensaje).layoutParams = newParams
            }
        }
    }
    //Cuantos elementos va a tener
    override fun getItemCount(): Int  = listaMensajes.size

    //Como se va a ver
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatViewHolder {
        return ChatViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.mensajes, parent, false)
        )
    }

    //Que va a tener
    override fun onBindViewHolder(holder: ChatViewHolder, position: Int) {
        holder.asignarInformacion(listaMensajes[position])
    }

}